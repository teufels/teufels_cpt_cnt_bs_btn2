<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelscptcntbsbtn2render',
	array(
		'Btn' => 'render',
		
	),
	// non-cacheable actions
	array(
		'Btn' => 'render',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelscptcntbsbtn2documentation',
	array(
		'Documentation' => 'render',
		
	),
	// non-cacheable actions
	array(
		'Documentation' => 'render',
		
	)
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY));
$pluginName = strtolower('Teufelscptcntbsbtn2render');
$pluginSignature = $extensionName.'_'.$pluginName;
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '
	mod {
    wizards.newContentElement.wizardItems.common {
      elements {
        teufels_cpt_cnt_bs_btn2 {
          icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_teufelscptcntbsbtn2_domain_model_btn.png
          title = Bootstrap Button
          description = Button(s) without Flux
          tt_content_defValues {
            CType = list
	     	list_type = ' . $pluginSignature . '
          }
        }
      }
      show := addToList(teufels_cpt_cnt_bs_btn2)
    }
  }
  '
);