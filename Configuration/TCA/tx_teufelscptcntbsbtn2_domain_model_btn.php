<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btn',
		'label' => 'backend_title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'backend_title,title,alternative,value,link,class_color,class_size,class_additional,class_glyphicon,justify,additional_params,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('teufels_cpt_cnt_bs_btn2') . 'Resources/Public/Icons/tx_teufelscptcntbsbtn2_domain_model_btn.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, backend_title, title, alternative, value, link, class_color, class_size, class_additional, class_glyphicon, justify, additional_params',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, backend_title, title, alternative, value, link, class_color, class_size, class_additional, class_glyphicon, justify, additional_params, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_teufelscptcntbsbtn2_domain_model_btn',
				'foreign_table_where' => 'AND tx_teufelscptcntbsbtn2_domain_model_btn.pid=###CURRENT_PID### AND tx_teufelscptcntbsbtn2_domain_model_btn.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'backend_title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btn.backend_title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btn.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'alternative' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btn.alternative',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'value' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btn.value',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'link' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btn.link',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'class_color' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btn.class_color',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'class_size' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btn.class_size',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'class_additional' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btn.class_additional',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'class_glyphicon' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btn.class_glyphicon',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'justify' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btn.justify',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'additional_params' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btn.additional_params',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		
	),
);## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder