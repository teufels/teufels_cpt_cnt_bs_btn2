<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btngroup',
		'label' => 'backend_title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'backend_title,dropdown_title,dropdown_header,dropdown,class_size,vertical,justify,btn_group,btn,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('teufels_cpt_cnt_bs_btn2') . 'Resources/Public/Icons/tx_teufelscptcntbsbtn2_domain_model_btngroup.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, backend_title, dropdown_title, dropdown_header, dropdown, class_size, vertical, justify, btn_group, btn',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, backend_title, dropdown_title, dropdown_header, dropdown, class_size, vertical, justify, btn_group, btn, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_teufelscptcntbsbtn2_domain_model_btngroup',
				'foreign_table_where' => 'AND tx_teufelscptcntbsbtn2_domain_model_btngroup.pid=###CURRENT_PID### AND tx_teufelscptcntbsbtn2_domain_model_btngroup.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'backend_title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btngroup.backend_title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'dropdown_title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btngroup.dropdown_title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'dropdown_header' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btngroup.dropdown_header',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'dropdown' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btngroup.dropdown',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'class_size' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btngroup.class_size',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('-- Label --', 0),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'vertical' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btngroup.vertical',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'justify' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btngroup.justify',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'btn_group' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btngroup.btn_group',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'foreign_table' => 'tx_teufelscptcntbsbtn2_domain_model_btngroup',
				'MM' => 'tx_teufelscptcntbsbtn2_btngroup_btngroup_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'module' => array(
							'name' => 'wizard_edit',
						),
						'type' => 'popup',
						'title' => 'Edit',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'module' => array(
							'name' => 'wizard_add',
						),
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_teufelscptcntbsbtn2_domain_model_btngroup',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
					),
				),
			),
		),
		'btn' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_db.xlf:tx_teufelscptcntbsbtn2_domain_model_btngroup.btn',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'foreign_table' => 'tx_teufelscptcntbsbtn2_domain_model_btn',
				'MM' => 'tx_teufelscptcntbsbtn2_btngroup_btn_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'module' => array(
							'name' => 'wizard_edit',
						),
						'type' => 'popup',
						'title' => 'Edit',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'module' => array(
							'name' => 'wizard_add',
						),
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_teufelscptcntbsbtn2_domain_model_btn',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
					),
				),
			),
		),
		
	),
);## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder