<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelscptcntbsbtn2render',
	'Button or Button Group'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelscptcntbsbtn2documentation',
	'Button :: User documentation'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'teufels_cpt_cnt_bs_btn2');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelscptcntbsbtn2_domain_model_btn', 'EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_csh_tx_teufelscptcntbsbtn2_domain_model_btn.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelscptcntbsbtn2_domain_model_btn');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelscptcntbsbtn2_domain_model_btngroup', 'EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_csh_tx_teufelscptcntbsbtn2_domain_model_btngroup.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelscptcntbsbtn2_domain_model_btngroup');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelscptcntbsbtn2_domain_model_documentation', 'EXT:teufels_cpt_cnt_bs_btn2/Resources/Private/Language/locallang_csh_tx_teufelscptcntbsbtn2_domain_model_documentation.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelscptcntbsbtn2_domain_model_documentation');
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY));
$pluginName = strtolower('Teufelscptcntbsbtn2render');
$pluginSignature = $extensionName.'_'.$pluginName;
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'.$_EXTKEY . '/Configuration/FlexForms/Config.xml');