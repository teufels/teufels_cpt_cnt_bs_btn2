﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
teufels_cpt_cnt_bs_btn2
=============================================================

.. only:: html

	:Classification:
		teufels_cpt_cnt_bs_btn2

	:Version:
		|release|

	:Language:
		en

	:Description:
		NO FLUX :)

	:Keywords:
		comma,separated,list,of,keywords

	:Copyright:
		2017

	:Author:
		Andreas Hafner, Dominik Hilser, Georg Kathan, Hendrik Krüger, Josymar Escalona Rodriguez, Perrin Ennen, Timo Bittner

	:Email:
		a.hafner@teufels.com, d.hilser@teufels.com, g.kathan@teufels.com, h.krueger@teufels.com, j.rodriguez@teufels.com, p.ennen@teufels.com, t.bittner@teufels.com

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 3
	:titlesonly:

	Introduction/Index
	User/Index
	Administrator/Index
	Configuration/Index
	Developer/Index
	KnownProblems/Index
	ToDoList/Index
	ChangeLog/Index
	Links
