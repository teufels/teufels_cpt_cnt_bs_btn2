<?php
namespace TEUFELS\TeufelsCptCntBsBtn2\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Btns
 */
class BtnRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );
    
    /**
     * findByUidListOrderByList
     *
     * @param string String containing uids
     * @return \TEUFELS\Sdg16ExtBox\Domain\Model\Boxlist
     */
    public function findByUidListOrderByList($uidList)
    {
        $uidArray = explode(',', $uidList);
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        //ignore storagePid
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->matching($query->in('uid', $uidArray), $query->logicalAnd($query->equals('hidden', 0), $query->equals('deleted', 0)));
        $query->setOrderings($this->orderByField('uid', $uidArray));
        return $query->execute();
    }
    
    /**
     * Set the order method
     *
     * @param $field
     * @param $values
     */
    protected function orderByField($field, $values)
    {
        $orderings = array();
        foreach ($values as $value) {
            $orderings["{$field}={$value}"] = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING;
        }
        return $orderings;
    }

}