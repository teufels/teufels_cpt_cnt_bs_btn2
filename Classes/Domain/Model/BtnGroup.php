<?php
namespace TEUFELS\TeufelsCptCntBsBtn2\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * BtnGroup
 */
class BtnGroup extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * backendTitle
     *
     * @var string
     * @validate NotEmpty
     */
    protected $backendTitle = '';
    
    /**
     * dropdownTitle
     *
     * @var string
     */
    protected $dropdownTitle = '';
    
    /**
     * dropdownHeader
     *
     * @var string
     */
    protected $dropdownHeader = '';
    
    /**
     * dropdown
     *
     * @var bool
     */
    protected $dropdown = false;
    
    /**
     * classSize
     *
     * @var int
     */
    protected $classSize = 0;
    
    /**
     * vertical
     *
     * @var bool
     */
    protected $vertical = false;
    
    /**
     * justify
     *
     * @var bool
     */
    protected $justify = false;
    
    /**
     * btnGroup
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup>
     */
    protected $btnGroup = null;
    
    /**
     * btn
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn>
     */
    protected $btn = null;
    
    /**
     * Returns the backendTitle
     *
     * @return string $backendTitle
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }
    
    /**
     * Sets the backendTitle
     *
     * @param string $backendTitle
     * @return void
     */
    public function setBackendTitle($backendTitle)
    {
        $this->backendTitle = $backendTitle;
    }
    
    /**
     * Returns the classSize
     *
     * @return int $classSize
     */
    public function getClassSize()
    {
        return $this->classSize;
    }
    
    /**
     * Sets the classSize
     *
     * @param int $classSize
     * @return void
     */
    public function setClassSize($classSize)
    {
        $this->classSize = $classSize;
    }
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->btnGroup = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->btn = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * Adds a BtnGroup
     *
     * @param \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup $btnGroup
     * @return void
     */
    public function addBtnGroup(\TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup $btnGroup)
    {
        $this->btnGroup->attach($btnGroup);
    }
    
    /**
     * Removes a BtnGroup
     *
     * @param \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup $btnGroupToRemove The BtnGroup to be removed
     * @return void
     */
    public function removeBtnGroup(\TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup $btnGroupToRemove)
    {
        $this->btnGroup->detach($btnGroupToRemove);
    }
    
    /**
     * Returns the btnGroup
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup> $btnGroup
     */
    public function getBtnGroup()
    {
        return $this->btnGroup;
    }
    
    /**
     * Sets the btnGroup
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup> $btnGroup
     * @return void
     */
    public function setBtnGroup(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $btnGroup)
    {
        $this->btnGroup = $btnGroup;
    }
    
    /**
     * Adds a Btn
     *
     * @param \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn $btn
     * @return void
     */
    public function addBtn(\TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn $btn)
    {
        $this->btn->attach($btn);
    }
    
    /**
     * Removes a Btn
     *
     * @param \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn $btnToRemove The Btn to be removed
     * @return void
     */
    public function removeBtn(\TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn $btnToRemove)
    {
        $this->btn->detach($btnToRemove);
    }
    
    /**
     * Returns the btn
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn> $btn
     */
    public function getBtn()
    {
        return $this->btn;
    }
    
    /**
     * Sets the btn
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn> $btn
     * @return void
     */
    public function setBtn(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $btn)
    {
        $this->btn = $btn;
    }
    
    /**
     * Returns the vertical
     *
     * @return bool $vertical
     */
    public function getVertical()
    {
        return $this->vertical;
    }
    
    /**
     * Sets the vertical
     *
     * @param bool $vertical
     * @return void
     */
    public function setVertical($vertical)
    {
        $this->vertical = $vertical;
    }
    
    /**
     * Returns the boolean state of vertical
     *
     * @return bool
     */
    public function isVertical()
    {
        return $this->vertical;
    }
    
    /**
     * Returns the justify
     *
     * @return bool $justify
     */
    public function getJustify()
    {
        return $this->justify;
    }
    
    /**
     * Sets the justify
     *
     * @param bool $justify
     * @return void
     */
    public function setJustify($justify)
    {
        $this->justify = $justify;
    }
    
    /**
     * Returns the boolean state of justify
     *
     * @return bool
     */
    public function isJustify()
    {
        return $this->justify;
    }
    
    /**
     * Returns the dropdown
     *
     * @return bool $dropdown
     */
    public function getDropdown()
    {
        return $this->dropdown;
    }
    
    /**
     * Sets the dropdown
     *
     * @param bool $dropdown
     * @return void
     */
    public function setDropdown($dropdown)
    {
        $this->dropdown = $dropdown;
    }
    
    /**
     * Returns the boolean state of dropdown
     *
     * @return bool
     */
    public function isDropdown()
    {
        return $this->dropdown;
    }
    
    /**
     * Returns the dropdownTitle
     *
     * @return string dropdownTitle
     */
    public function getDropdownTitle()
    {
        return $this->dropdownTitle;
    }
    
    /**
     * Sets the dropdownTitle
     *
     * @param string $dropdownTitle
     * @return void
     */
    public function setDropdownTitle($dropdownTitle)
    {
        $this->dropdownTitle = $dropdownTitle;
    }
    
    /**
     * Returns the dropdownHeader
     *
     * @return string $dropdownHeader
     */
    public function getDropdownHeader()
    {
        return $this->dropdownHeader;
    }
    
    /**
     * Sets the dropdownHeader
     *
     * @param string $dropdownHeader
     * @return void
     */
    public function setDropdownHeader($dropdownHeader)
    {
        $this->dropdownHeader = $dropdownHeader;
    }

}