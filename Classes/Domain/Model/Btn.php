<?php
namespace TEUFELS\TeufelsCptCntBsBtn2\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Btn
 */
class Btn extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * backendTitle
     *
     * @var string
     * @validate NotEmpty
     */
    protected $backendTitle = '';
    
    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';
    
    /**
     * alternative
     *
     * @var string
     * @validate NotEmpty
     */
    protected $alternative = '';
    
    /**
     * value
     *
     * @var string
     * @validate NotEmpty
     */
    protected $value = '';
    
    /**
     * link
     *
     * @var string
     * @validate NotEmpty
     */
    protected $link = '';
    
    /**
     * classColor
     *
     * @var string
     */
    protected $classColor = '';
    
    /**
     * classSize
     *
     * @var string
     */
    protected $classSize = '';
    
    /**
     * classAdditional
     *
     * @var string
     */
    protected $classAdditional = '';
    
    /**
     * classGlyphicon
     *
     * @var string
     */
    protected $classGlyphicon = '';
    
    /**
     * justify
     *
     * @var bool
     */
    protected $justify = false;
    
    /**
     * additionalParams
     *
     * @var string
     */
    protected $additionalParams = '';
    
    /**
     * Returns the backendTitle
     *
     * @return string $backendTitle
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }
    
    /**
     * Sets the backendTitle
     *
     * @param string $backendTitle
     * @return void
     */
    public function setBackendTitle($backendTitle)
    {
        $this->backendTitle = $backendTitle;
    }
    
    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * Returns the alternative
     *
     * @return string $alternative
     */
    public function getAlternative()
    {
        return $this->alternative;
    }
    
    /**
     * Sets the alternative
     *
     * @param string $alternative
     * @return void
     */
    public function setAlternative($alternative)
    {
        $this->alternative = $alternative;
    }
    
    /**
     * Returns the value
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Sets the value
     *
     * @param string $value
     * @return void
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
    
    /**
     * Returns the boolean state of classBlock
     *
     * @return bool
     */
    public function isClassBlock()
    {
        return $this->classBlock;
    }
    
    /**
     * Returns the link
     *
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }
    
    /**
     * Sets the link
     *
     * @param string $link
     * @return void
     */
    public function setLink($link)
    {
        $this->link = $link;
    }
    
    /**
     * Returns the classAdditional
     *
     * @return string $classAdditional
     */
    public function getClassAdditional()
    {
        return $this->classAdditional;
    }
    
    /**
     * Sets the classAdditional
     *
     * @param string $classAdditional
     * @return void
     */
    public function setClassAdditional($classAdditional)
    {
        $this->classAdditional = $classAdditional;
    }
    
    /**
     * Returns the justify
     *
     * @return bool justify
     */
    public function getJustify()
    {
        return $this->justify;
    }
    
    /**
     * Sets the justify
     *
     * @param bool $justify
     * @return void
     */
    public function setJustify($justify)
    {
        $this->justify = $justify;
    }
    
    /**
     * Returns the classColor
     *
     * @return string classColor
     */
    public function getClassColor()
    {
        return $this->classColor;
    }
    
    /**
     * Sets the classColor
     *
     * @param int $classColor
     * @return void
     */
    public function setClassColor($classColor)
    {
        $this->classColor = $classColor;
    }
    
    /**
     * Returns the classSize
     *
     * @return string classSize
     */
    public function getClassSize()
    {
        return $this->classSize;
    }
    
    /**
     * Sets the classSize
     *
     * @param int $classSize
     * @return void
     */
    public function setClassSize($classSize)
    {
        $this->classSize = $classSize;
    }
    
    /**
     * Returns the classGlyphicon
     *
     * @return string classGlyphicon
     */
    public function getClassGlyphicon()
    {
        return $this->classGlyphicon;
    }
    
    /**
     * Sets the classGlyphicon
     *
     * @param int $classGlyphicon
     * @return void
     */
    public function setClassGlyphicon($classGlyphicon)
    {
        $this->classGlyphicon = $classGlyphicon;
    }
    
    /**
     * Returns the additionalParams
     *
     * @return string $additionalParams
     */
    public function getAdditionalParams()
    {
        return $this->additionalParams;
    }
    
    /**
     * Sets the additionalParams
     *
     * @param string $additionalParams
     * @return void
     */
    public function setAdditionalParams($additionalParams)
    {
        $this->additionalParams = $additionalParams;
    }

}