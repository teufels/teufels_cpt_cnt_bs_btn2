<?php
namespace TEUFELS\TeufelsCptCntBsBtn2\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * BtnController
 */
class BtnController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * btnRepository
     *
     * @var \TEUFELS\TeufelsCptCntBsBtn2\Domain\Repository\BtnRepository
     * @inject
     */
    protected $btnRepository = NULL;
    
    /**
     * action render
     *
     * @return void
     */
    public function renderAction()
    {
        $aSettings = $this->settings;
        if (array_key_exists('bDebug', $aSettings) && array_key_exists('sDebugIp', $aSettings)) {
            $aDebugIp = explode(',', $aSettings['sDebugIp']);
            if ($aSettings['bDebug'] == '1' && (in_array($_SERVER['REMOTE_ADDR'], $aDebugIp) or $aSettings['sDebugIp'] == '*')) {
                \TYPO3\CMS\Core\Utility\DebugUtility::debug($aSettings, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
            }
        }
        $oButtons = $this->btnRepository->findByUidListOrderByList($aSettings['oTeufelsCptCntBsBtn2']);
        $this->view->assign('oButtons', $oButtons);
    }

}