<?php

namespace TEUFELS\TeufelsCptCntBsBtn2\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class BtnGroupTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getBackendTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getBackendTitle()
		);
	}

	/**
	 * @test
	 */
	public function setBackendTitleForStringSetsBackendTitle()
	{
		$this->subject->setBackendTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'backendTitle',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDropdownTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getDropdownTitle()
		);
	}

	/**
	 * @test
	 */
	public function setDropdownTitleForStringSetsDropdownTitle()
	{
		$this->subject->setDropdownTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'dropdownTitle',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDropdownHeaderReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getDropdownHeader()
		);
	}

	/**
	 * @test
	 */
	public function setDropdownHeaderForStringSetsDropdownHeader()
	{
		$this->subject->setDropdownHeader('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'dropdownHeader',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDropdownReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getDropdown()
		);
	}

	/**
	 * @test
	 */
	public function setDropdownForBoolSetsDropdown()
	{
		$this->subject->setDropdown(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'dropdown',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getClassSizeReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setClassSizeForIntSetsClassSize()
	{	}

	/**
	 * @test
	 */
	public function getVerticalReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getVertical()
		);
	}

	/**
	 * @test
	 */
	public function setVerticalForBoolSetsVertical()
	{
		$this->subject->setVertical(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'vertical',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getJustifyReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getJustify()
		);
	}

	/**
	 * @test
	 */
	public function setJustifyForBoolSetsJustify()
	{
		$this->subject->setJustify(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'justify',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getBtnGroupReturnsInitialValueForBtnGroup()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getBtnGroup()
		);
	}

	/**
	 * @test
	 */
	public function setBtnGroupForObjectStorageContainingBtnGroupSetsBtnGroup()
	{
		$btnGroup = new \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup();
		$objectStorageHoldingExactlyOneBtnGroup = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneBtnGroup->attach($btnGroup);
		$this->subject->setBtnGroup($objectStorageHoldingExactlyOneBtnGroup);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneBtnGroup,
			'btnGroup',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addBtnGroupToObjectStorageHoldingBtnGroup()
	{
		$btnGroup = new \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup();
		$btnGroupObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$btnGroupObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($btnGroup));
		$this->inject($this->subject, 'btnGroup', $btnGroupObjectStorageMock);

		$this->subject->addBtnGroup($btnGroup);
	}

	/**
	 * @test
	 */
	public function removeBtnGroupFromObjectStorageHoldingBtnGroup()
	{
		$btnGroup = new \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\BtnGroup();
		$btnGroupObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$btnGroupObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($btnGroup));
		$this->inject($this->subject, 'btnGroup', $btnGroupObjectStorageMock);

		$this->subject->removeBtnGroup($btnGroup);

	}

	/**
	 * @test
	 */
	public function getBtnReturnsInitialValueForBtn()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getBtn()
		);
	}

	/**
	 * @test
	 */
	public function setBtnForObjectStorageContainingBtnSetsBtn()
	{
		$btn = new \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn();
		$objectStorageHoldingExactlyOneBtn = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneBtn->attach($btn);
		$this->subject->setBtn($objectStorageHoldingExactlyOneBtn);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneBtn,
			'btn',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addBtnToObjectStorageHoldingBtn()
	{
		$btn = new \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn();
		$btnObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$btnObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($btn));
		$this->inject($this->subject, 'btn', $btnObjectStorageMock);

		$this->subject->addBtn($btn);
	}

	/**
	 * @test
	 */
	public function removeBtnFromObjectStorageHoldingBtn()
	{
		$btn = new \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn();
		$btnObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$btnObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($btn));
		$this->inject($this->subject, 'btn', $btnObjectStorageMock);

		$this->subject->removeBtn($btn);

	}
}
