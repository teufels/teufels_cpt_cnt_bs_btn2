<?php

namespace TEUFELS\TeufelsCptCntBsBtn2\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class BtnTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \TEUFELS\TeufelsCptCntBsBtn2\Domain\Model\Btn();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getBackendTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getBackendTitle()
		);
	}

	/**
	 * @test
	 */
	public function setBackendTitleForStringSetsBackendTitle()
	{
		$this->subject->setBackendTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'backendTitle',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle()
	{
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAlternativeReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getAlternative()
		);
	}

	/**
	 * @test
	 */
	public function setAlternativeForStringSetsAlternative()
	{
		$this->subject->setAlternative('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'alternative',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getValueReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getValue()
		);
	}

	/**
	 * @test
	 */
	public function setValueForStringSetsValue()
	{
		$this->subject->setValue('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'value',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getLinkReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getLink()
		);
	}

	/**
	 * @test
	 */
	public function setLinkForStringSetsLink()
	{
		$this->subject->setLink('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'link',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getClassColorReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getClassColor()
		);
	}

	/**
	 * @test
	 */
	public function setClassColorForStringSetsClassColor()
	{
		$this->subject->setClassColor('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'classColor',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getClassSizeReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getClassSize()
		);
	}

	/**
	 * @test
	 */
	public function setClassSizeForStringSetsClassSize()
	{
		$this->subject->setClassSize('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'classSize',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getClassAdditionalReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getClassAdditional()
		);
	}

	/**
	 * @test
	 */
	public function setClassAdditionalForStringSetsClassAdditional()
	{
		$this->subject->setClassAdditional('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'classAdditional',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getClassGlyphiconReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getClassGlyphicon()
		);
	}

	/**
	 * @test
	 */
	public function setClassGlyphiconForStringSetsClassGlyphicon()
	{
		$this->subject->setClassGlyphicon('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'classGlyphicon',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getJustifyReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getJustify()
		);
	}

	/**
	 * @test
	 */
	public function setJustifyForBoolSetsJustify()
	{
		$this->subject->setJustify(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'justify',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAdditionalParamsReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getAdditionalParams()
		);
	}

	/**
	 * @test
	 */
	public function setAdditionalParamsForStringSetsAdditionalParams()
	{
		$this->subject->setAdditionalParams('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'additionalParams',
			$this->subject
		);
	}
}
