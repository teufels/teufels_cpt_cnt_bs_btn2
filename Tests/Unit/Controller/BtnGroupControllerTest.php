<?php
namespace TEUFELS\TeufelsCptCntBsBtn2\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *  			Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *  			Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *  			Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *  			Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *  			Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *  			Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class TEUFELS\TeufelsCptCntBsBtn2\Controller\BtnGroupController.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class BtnGroupControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \TEUFELS\TeufelsCptCntBsBtn2\Controller\BtnGroupController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('TEUFELS\\TeufelsCptCntBsBtn2\\Controller\\BtnGroupController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

}
